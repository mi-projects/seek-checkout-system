# Overview 
This project is a sample checkout system for SEEK which implements various pricing rules for a small number of privileged customers.
This is a back-end solution and does not focus on front-end skills and the web pages provided are only for demo purpose.

## Motivation
This is to demonstrate my coding and problem solving style.

## Installing
```
npm install
```

## Running
    1. npm start
    2. For demo purpose go to : http://localhost:3000
    3. A Postman collection (SEEK.postman_collection.json) is also provided in the project
       folder to debug GETs and POSTs requests.

## Testing
### Unit Testing
```
npm test -t .\test\discount-test.js
```
### Blackbox Testing
```
npm test -t .\test\server-test.js
```
### Overall Testing
```
npm test
```
### Code Coverage Testing
```
npm run coverage
.\coverage\lcov-report\index.html
```