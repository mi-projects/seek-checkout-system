module.exports = function Cart(oldCart) {
    
    this.items = oldCart.items || {};
    this.totalQty = oldCart.totalQty || 0;
    this.totalPrice = oldCart.totalPrice || 0;

    this.add = function (item, id) {
        var storedItem = this.items[id];
        if (!storedItem) {
            storedItem = this.items[id] = {product: item, qty: 0 , price: 0};
        }
        storedItem.qty++;
        storedItem.price = storedItem.product.price * storedItem.qty;
        this.totalQty++;
        this.totalPrice += storedItem.product.price;
    };

    this.generateArray = function() {
        var arr = [];
        for (var id in this.items) {
            arr.push(this.items[id]);
        }
        return arr;
    };
    
    this.getTotalPrice = function() {
        return this.totalPrice.toFixed(2);
    }
};