var express = require('express');
var router = express.Router();
var sampleProducts = require('../sample-data/sample-product');
var sampleCustomers = require('../sample-data/sample-customer');


/* GET customers page. */
router.get('/customer', function(req, res, next) {

  var customers = [];
  for (const customer of Object.keys(sampleCustomers)) {
    customers.push(sampleCustomers[customer]);
  }
  res.render('sample-views/customer', { title: 'SEEK - Customer', customers: customers });
});

/* GET products page. */
router.get('/product', function(req, res, next) {
  var products = [];
  for (const product of Object.keys(sampleProducts)) {
    products.push(sampleProducts[product]);
  }
  res.render('sample-views/product', { title: 'SEEK - Product', products: products });
});

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('index', { title: 'SEEK CHECKOUT SYSTEM'});

});

module.exports = router;
