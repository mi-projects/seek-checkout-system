var express = require('express');
var router = express.Router();
var DiscountCalculator = require('../discount/discount-calculator');
var customers = require('../sample-data/sample-customer');

/* POST Customer ID Including Shopping Cart and Return Updated Shopping Cart with Discount Amount Calculated*/
router.post('/checkout', function(req, res, next) {
 
  if (!req.body.id) {
    return res.json({message: "Couldn't find customer id!"});
  }
  var id = req.body.id;

  var customer = customers[id]
  if (!customer) {
    return res.json({message: "Invalid customer id!"});
  }

  var shoppingCart = req.body.shoppingCart;
  if (!shoppingCart || !shoppingCart.hasOwnProperty('items')) {
     return res.json({message: "No shopping cart found!"});
  }
 
  var discountCalculator = new DiscountCalculator(customer, shoppingCart.items);
  var discountAmount = discountCalculator.applyPolicy();
  var finalPrice =  (shoppingCart.totalPrice - discountAmount).toFixed(2);

  var data = {
    cartTitle: customer.name + ' CART',
    cart: shoppingCart.items, 
    totalPrice: parseFloat(shoppingCart.totalPrice),  
    discountAmount: parseFloat(discountAmount),
    finalPrice: parseFloat(finalPrice)
  }
  res.json(data);
  
});

/*=============== For Demo Only =================*/
router.get('/:id', function(req, res, next) {
 
  var id = req.params.id;
  var customer = customers[id];
  var shoppingCart = customer.shoppingCart;
 
  var discountCalculator = new DiscountCalculator(customer, shoppingCart.items);
  var discountAmount = discountCalculator.applyPolicy();
  var finalPrice =  (shoppingCart.totalPrice - discountAmount).toFixed(2);

  var data = {
    cartTitle: customer.name + ' CART',
    cart: shoppingCart.items, 
    totalPrice: parseFloat(shoppingCart.totalPrice),  
    discountAmount: parseFloat(discountAmount),
    finalPrice: parseFloat(finalPrice)
  }
  res.render('sample-views/cart', data);
  
});

router.get('/' , function(req, res, next) {
  res.render('sample-views/cart-list', { title: 'SAMPLE CARTS'});
});
/*================================================*/

module.exports = router;
