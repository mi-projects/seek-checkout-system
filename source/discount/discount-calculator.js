var DiscountPolicy = require('../discount/discount-policy');

module.exports = function DiscountCalculator(customer, items) {
    this.customer = customer || {};
    this.items = items || {};
    this.totalDiscount = 0;
    
    /** N for One Less Policy */
     this.getNforOneLess = function (n, discount) {  
        var productID = discount.product;
        var itemQty = 0;
        var itemPrice = 0;
        var totalPrice = 0;
        var totalQty = 0; 

        for (var item of this.items) {
            console.log(item);
            if (item.product.id == productID) {
                itemQty = item.qty;
                itemPrice = item.product.price;
            }
            totalQty += item.qty;
            totalPrice += item.price;
        }

        var freeItem = parseInt(itemQty / n);
        var totalDiscount = freeItem * itemPrice;
        this.totalDiscount += totalDiscount;

        console.log('Discount: ' + totalDiscount);
    };

    /** N or More Policy */
     this.nOrMore = function (n, newPrice, discount) {
        var productID = discount.product;
        var itemQty = 0;
        var itemPrice = 0;
        var totalPrice = 0;
        var totalQty = 0;
      
        for (var item of this.items) {
            console.log(item);
            if (item.product.id == productID) {
                itemQty = item.qty;
                itemPrice = item.product.price;
            }
            totalPrice += item.price;
            totalQty += item.qty;
        }
        if (itemQty >= n) {
            var totalDiscount = itemQty * (itemPrice - newPrice);
            this.totalDiscount += totalDiscount;
            console.log('Discount: ' + totalDiscount);
        }

     };
   
    /** Fix Price Policy */
    this.getFixedPrice = function (newPrice, discount) {
        var productID = discount.product;
        var itemQty = 0;
        var itemPrice = 0;
        var totalPrice = 0;
        var totalQty = 0;

        for (var item of this.items) {
            console.log(item);
            if (item.product.id == productID) {
                itemQty = item.qty;
                itemPrice = item.product.price;
            }
            totalPrice += item.price;
            totalQty += item.qty;
        }
      
        var totalDiscount = itemQty * (itemPrice - newPrice);
        this.totalDiscount += totalDiscount;
        console.log('Discount: ' + totalDiscount);
    };


    /** Apply All Discount Policies */
    this.applyPolicy = function() {
        console.log('=========================' + this.customer.name +  ' CART ==========================');
        if (this.customer.discount) {
            for (var discount of this.customer.discount) {
                var type = discount.pricePolicy || -1;
                var num = discount.n || 0;
                var price = discount.price || 0;
                
                switch (type) {  
                    case DiscountPolicy['Discount'].type.n_for_one_less:
                        console.log('Discount Policy: N-for-One-Less');
                        this.getNforOneLess(num, discount);
                        break;
                    case DiscountPolicy['Discount'].type.n_or_more:
                        console.log('Discount Policy: N-or-More');
                        this.nOrMore(num, price, discount);
                        break;
                    case DiscountPolicy['Discount'].type.fixed_price:
                        console.log('Discount Policy: Fixed-Price');
                        this.getFixedPrice(price, discount);
                        break;
                    default:
                        console.log('Discount Policy: Default');
                        break;
                }
            }
        } 
        return (this.totalDiscount);
    };
};

