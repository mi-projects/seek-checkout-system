exports.Discount =  {
        type: {
            n_for_one_less: 'N-For-One-Less',
            n_or_more: 'N-or-More',
            fixed_price: 'Fixed-Price' 
        },
        n: 0,
        price: 0,
        product: ''
}