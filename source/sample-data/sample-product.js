/*============== Sample Products ==============*/

var Product = require('../models/product');

var sampleProducts = {
    'classic' : new Product(
        'classic',
        'Classic Ad',
        269.99
    ),
    'standout': new Product(
        'standout',
        'Standout Ad',
        322.99
    ),
    'premium': new Product(
        'premium',
        'Premium Ad',
        394.99
    ),

};

module.exports = sampleProducts;