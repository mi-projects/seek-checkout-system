/*============== Sample Customers ==============*/

var Customer = require('../models/customer');
var DiscountPolicy = require('../discount/discount-policy');

var unileverCart = require('./sample-cart/unilever-cart');
var appleCart = require('./sample-cart/apple-cart');
var nikeCart = require('./sample-cart/nike-cart');
var fordCart = require('./sample-cart/ford-cart');
var defaultCart = require('./sample-cart/default-cart');

var customers = {
    'unilever' : new Customer(
        'UNILEVER',
        [
            { 
                pricePolicy: DiscountPolicy['Discount'].type.n_for_one_less,
                n: 3, 
                product: 'classic', 
            }
        ],
        unileverCart
    ),
    'apple' : new Customer(
        'APPLE',
        [
            {
                pricePolicy: DiscountPolicy['Discount'].type.fixed_price,
                price: 299.99, 
                product: 'standout'
            }
        ],
        appleCart
    ),
    'nike': new Customer(
        'NIKE',
         [
            {
                pricePolicy: DiscountPolicy['Discount'].type.n_or_more,
                price: 379.99, 
                n: 4,
                product: 'premium'
            }
        ],
        nikeCart
    ),
    'ford': new Customer(
        'FORD',
         [
            { 
                pricePolicy: DiscountPolicy['Discount'].type.n_for_one_less,
                n: 5, 
                product: 'classic', 
            },
             {
                pricePolicy: DiscountPolicy['Discount'].type.fixed_price,
                price: 309.99, 
                product: 'standout'
            },
            {
                pricePolicy: DiscountPolicy['Discount'].type.n_or_more,
                price: 389.99, 
                n: 3,
                product: 'premium'
            }
        ],
        fordCart
    ),
    'default': new Customer(
        'DEFAULT',
        [],
        defaultCart
    ),

};

module.exports = customers;