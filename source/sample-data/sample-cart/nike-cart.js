/*============== Nike - Sample Shopping Cart ==============*/

var products = require('../sample-product');
var Cart = require('../../models/cart');

var nikeCart = new Cart({});

nikeCart.add(products.premium, products.premium.id);
nikeCart.add(products.premium, products.premium.id);
nikeCart.add(products.premium, products.premium.id);
nikeCart.add(products.premium, products.premium.id);

var items = nikeCart.generateArray();
var totalPrice = nikeCart.getTotalPrice();

exports.items = items;
exports.totalPrice = totalPrice;
