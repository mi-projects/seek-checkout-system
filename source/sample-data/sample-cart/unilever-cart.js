/*============== Unilever - Sample Shopping Cart ==============*/

var products = require('../sample-product');
var Cart = require('../../models/cart');

var unileverCart = new Cart({});

unileverCart.add(products.classic, products.classic.id);
unileverCart.add(products.classic, products.classic.id);
unileverCart.add(products.classic, products.classic.id);
unileverCart.add(products.premium, products.premium.id);

var items = unileverCart.generateArray();
var totalPrice = unileverCart.getTotalPrice();

exports.items = items;
exports.totalPrice = totalPrice;