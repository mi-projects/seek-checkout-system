/*============== Ford - Sample Shopping Cart ==============*/

var products = require('../sample-product');
var Cart = require('../../models/cart');

var fordCart = new Cart({});

fordCart.add(products.classic, products.classic.id);
fordCart.add(products.classic, products.classic.id);
fordCart.add(products.classic, products.classic.id);
fordCart.add(products.classic, products.classic.id);
fordCart.add(products.classic, products.classic.id);

fordCart.add(products.standout, products.standout.id);
fordCart.add(products.standout, products.standout.id);

fordCart.add(products.premium, products.premium.id);
fordCart.add(products.premium, products.premium.id);
fordCart.add(products.premium, products.premium.id);
fordCart.add(products.premium, products.premium.id);


var items = fordCart.generateArray();
var totalPrice = fordCart.getTotalPrice();

exports.items = items;
exports.totalPrice = totalPrice;
