/*============== Default - Sample Shopping Cart ==============*/

var products = require('../sample-product');
var Cart = require('../../models/cart');

var defaultCart = new Cart({});

defaultCart.add(products.classic, products.classic.id);
defaultCart.add(products.standout, products.standout.id);
defaultCart.add(products.premium, products.premium.id);

var items = defaultCart.generateArray();
var totalPrice = defaultCart.getTotalPrice();

exports.items = items;
exports.totalPrice = totalPrice;

