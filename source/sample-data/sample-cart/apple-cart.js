/*============== Apple - Sample Shopping Cart ==============*/

var products = require('../sample-product');
var Cart = require('../../models/cart');

var appleCart = new Cart({});

appleCart.add(products.standout, products.standout.id);
appleCart.add(products.standout, products.standout.id);
appleCart.add(products.standout, products.standout.id);
appleCart.add(products.premium, products.premium.id);

var items = appleCart.generateArray();
var totalPrice = appleCart.getTotalPrice();

exports.items = items;
exports.totalPrice = totalPrice;