/*============== Apple - Test Data ==============*/

exports.appleTestData = {
    "id": "apple",
    "name": "APPLE",
    "discount": [
        {
            "pricePolicy": "Fixed-Price",
            "price": 299.99,
            "product": "standout"
        }
    ],
    "shoppingCart": {
        "items": [
            {
                "product": {
                    "id": "standout",
                    "name": "Standout Ad",
                    "price": 322.99
                },
                "qty": 3,
                "price": 968.97
            },
            {
                "product": {
                    "id": "premium",
                    "name": "Premium Ad",
                    "price": 394.99
                },
                "qty": 1,
                "price": 394.99
            }
        ],
        "totalPrice": "1363.96"
    }
}