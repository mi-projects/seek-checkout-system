/*============== Default - Test Data ==============*/

exports.defaultTestData = {
    "id": "default",
    "name": "DEFAULT",
    "discount": [
        {}
    ],
    "shoppingCart": {
        "items": [
            {
                "product": {
                    "id": "classic",
                    "name": "Classic Ad",
                    "price": 269.99
                },
                "qty": 1,
                "price": 269.99
            },
            {
                "product": {
                    "id": "standout",
                    "name": "Standout Ad",
                    "price": 322.99
                },
                "qty": 1,
                "price": 322.99
            },
            {
                "product": {
                    "id": "premium",
                    "name": "Premium Ad",
                    "price": 394.99
                },
                "qty": 1,
                "price": 394.99
            }
        ],
      "totalPrice": "987.97"
    }
}