/*============== Invalid Customer - Test Data ==============*/

var invalidId = {  
    "id": "invalidCustomer",
    "name": "Invalid Customer",
        "discount": [
            {
                "pricePolicy": "N-For-One-Less",
                "n": 3,
                "product": "classic"
            }
        ],
        "shoppingCart": {
            "items": [
                {
                    "product": {
                        "id": "classic",
                        "name": "Classic Ad",
                        "price": 269.99
                    },
                    "qty": 3,
                    "price": 809.97
                },
                {
                    "product": {
                        "id": "premium",
                        "name": "Premium Ad",
                        "price": 394.99
                    },
                    "qty": 1,
                    "price": 394.99
                }
            ],
            "totalPrice": "1204.96"
        }
}

var blankId = {  
    "name": "Invalid Customer",
        "discount": [
            {
                "pricePolicy": "N-For-One-Less",
                "n": 3,
                "product": "classic"
            }
        ],
        "shoppingCart": {
            "items": [
                {
                    "product": {
                        "id": "classic",
                        "name": "Classic Ad",
                        "price": 269.99
                    },
                    "qty": 3,
                    "price": 809.97
                },
                {
                    "product": {
                        "id": "premium",
                        "name": "Premium Ad",
                        "price": 394.99
                    },
                    "qty": 1,
                    "price": 394.99
                }
            ],
            "totalPrice": "1204.96"
        }
}

var emptyshoppingCart = {  
    "id": "default",
    "name": "DEFAULT",
        "discount": [
            {
            }
        ],
        "shoppingCart": {
        }
}

exports.invalidId = invalidId;
exports.blankId = blankId;
exports.emptyshoppingCart = emptyshoppingCart;
