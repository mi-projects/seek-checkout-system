/*============== Nike - Test Data ==============*/

exports.nikeTestData = {
    "id": "nike",
    "name": "NIKE",
    "discount": [
        {
            "pricePolicy": "N-or-More",
            "price": 379.99,
            "n": 4,
            "product": "premium"
        }
    ],
    "shoppingCart": {
        "items": [
            {
                "product": {
                    "id": "premium",
                    "name": "Premium Ad",
                    "price": 394.99
                },
                "qty": 4,
                "price": 1579.96
            }
        ],
        "totalPrice": "1579.96"
    }
}