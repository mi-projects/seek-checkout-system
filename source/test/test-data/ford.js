/*============== Ford - Test Data ==============*/

exports.fordTestData = {
    "id": "ford",
    "name": "FORD",
    "discount": [
        {
            "pricePolicy": "N-For-One-Less",
            "n": 5,
            "product": "classic"
        },
        {
            "pricePolicy": "Fixed-Price",
            "price": 309.99,
            "product": "standout"
        },
        {
            "pricePolicy": "N-or-More",
            "price": 389.99,
            "n": 3,
            "product": "premium"
        }
    ],
    "shoppingCart": {
        "items": [
            {
                "product": {
                    "id": "classic",
                    "name": "Classic Ad",
                    "price": 269.99
                },
                "qty": 5,
                "price": 1349.95
            },
            {
                "product": {
                    "id": "standout",
                    "name": "Standout Ad",
                    "price": 322.99
                },
                "qty": 2,
                "price": 645.98
            },
            {
                "product": {
                    "id": "premium",
                    "name": "Premium Ad",
                    "price": 394.99
                },
                "qty": 4,
                "price": 1579.96
            }
        ],
        "totalPrice": "3575.89"
    }
}