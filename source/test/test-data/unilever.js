/*============== Unilever - Test Data ==============*/

exports.unileverTestData = {  
    "id": "unilever",
    "name": "UNILEVER",
        "discount": [
            {
                "pricePolicy": "N-For-One-Less",
                "n": 3,
                "product": "classic"
            }
        ],
        "shoppingCart": {
            "items": [
                {
                    "product": {
                        "id": "classic",
                        "name": "Classic Ad",
                        "price": 269.99
                    },
                    "qty": 3,
                    "price": 809.97
                },
                {
                    "product": {
                        "id": "premium",
                        "name": "Premium Ad",
                        "price": 394.99
                    },
                    "qty": 1,
                    "price": 394.99
                }
            ],
            "totalPrice": "1204.96"
        }
}
