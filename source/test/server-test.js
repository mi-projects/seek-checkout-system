/*============== Blackbox Test ==============*/

var app = require('../app');
var request = require('request');
var chai  = require('chai'); 
var chaiHttp = require('chai-http');
var should = chai.should();

var unilever = require('./test-data/unilever');
var apple = require('./test-data/apple');
var nike = require('./test-data/nike');
var ford = require('./test-data/ford');
var defaultCustomer = require('./test-data/default');
var invalidCustomer = require('./test-data/invalid-customer')

chai.use(chaiHttp);

/* =========================== POST Requests Test ================================== */
  describe('/POST unilever', () => {
      it("should return UNILEVER shopping cart updated with discout amount and final price", (done) => {
        var customer = unilever.unileverTestData;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('cartTitle').that.is.a('string');
                res.body.should.have.property('cart').that.is.an('array');
                res.body.should.have.property('totalPrice').that.is.a('number').is.eql(1204.96);  
                res.body.should.have.property('discountAmount').that.is.a('number').is.eql(269.99);
                res.body.should.have.property('finalPrice').that.is.a('number').is.eql(934.97);         
              done();
            });
      });
  });

  describe('/POST apple', () => {
      it("should return APPLE shopping cart updated with discout amount and final price", (done) => {
        var customer = apple.appleTestData;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('cartTitle').that.is.a('string');
                res.body.should.have.property('cart').that.is.an('array');
                res.body.should.have.property('totalPrice').that.is.a('number').is.eql(1363.96);  
                res.body.should.have.property('discountAmount').that.is.a('number').is.eql(69);
                res.body.should.have.property('finalPrice').that.is.a('number').is.eql(1294.96);         
              done();
            });
      });
  });

   describe('/POST nike', () => {
      it("should return NIKE shopping cart updated with discout amount and final price", (done) => {
        var customer = nike.nikeTestData;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('cartTitle').that.is.a('string');
                res.body.should.have.property('cart').that.is.an('array');
                res.body.should.have.property('totalPrice').that.is.a('number').is.eql(1579.96);  
                res.body.should.have.property('discountAmount').that.is.a('number').is.eql(60);
                res.body.should.have.property('finalPrice').that.is.a('number').is.eql(1519.96);         
              done();
            });
      });
  });

  describe('/POST ford', () => {
      it("should return FORD shopping cart updated with discout amount and final price", (done) => {
        var customer = ford.fordTestData;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('cartTitle').that.is.a('string');
                res.body.should.have.property('cart').that.is.an('array');
                res.body.should.have.property('totalPrice').that.is.a('number').is.eql(3575.89);  
                res.body.should.have.property('discountAmount').that.is.a('number').is.eql(315.99);
                res.body.should.have.property('finalPrice').that.is.a('number').is.eql(3259.90);         
              done();
            });
      });
  });

  describe('/POST defaultCustomer', () => {
      it("should return DEFAULT shopping cart updated with no discount and same total price", (done) => {
        var customer = defaultCustomer.defaultTestData;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('cartTitle').that.is.a('string');
                res.body.should.have.property('cart').that.is.an('array');
                res.body.should.have.property('totalPrice').that.is.a('number').is.eql(987.97);  
                res.body.should.have.property('discountAmount').that.is.a('number').is.eql(0);
                res.body.should.have.property('finalPrice').that.is.a('number').is.eql(987.97);         
              done();
            });
      });
  });

  describe('/POST Invalid Customer', () => {
      it("it shouldn't POST customer with no id", (done) => {
        var customer = invalidCustomer.blankId;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('message').that.is.a('string').is.eql("Couldn't find customer id!");         
              done();
            });
      });

      it("it shouldn't POST invalid customer", (done) => {
        var customer = invalidCustomer.invalidId;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('message').that.is.a('string').is.eql("Invalid customer id!");         
              done();
            });
      });
  });

describe('/POST Customer with empty shopping cart', () => {
      it("it shouldn't POST customer with empty shopping cart", (done) => {
        var customer = invalidCustomer.emptyshoppingCart;
        chai.request(app)
            .post('/sample-cart/checkout')
            .send(customer)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');    
                res.body.should.have.property('message').that.is.a('string').is.eql("No shopping cart found!");         
              done();
            });
      });
});
/* =========================== GET Requests Test ================================== */
describe('/GET /', () => {
      it('should get links to products, customers and sample carts', (done) => {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.html;
                res.text.should.match(/SEEK CHECKOUT SYSTEM/);
                res.text.should.match(/Sample Products/);
                res.text.should.match(/Sample Customers/);
                res.text.should.match(/Sample Carts/);
              done();
            });
      });
  });

describe('/GET sample-cart', () => {
      it('it should GET list of sample carts', (done) => {
        chai.request(app)
            .get('/sample-cart')
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.html;
                res.text.should.match(/SAMPLE CARTS/);
                res.text.should.match(/Unilever Cart/);
                res.text.should.match(/Apple Cart/);
                res.text.should.match(/Nike Cart/);
                res.text.should.match(/Ford Cart/);
                res.text.should.match(/Default Cart/);
              done();
            });
      });
  });

  describe('/GET customer', () => {
      it('it should GET all customers', (done) => {
        chai.request(app)
            .get('/customer')
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.html;
                res.text.should.match(/UNILEVER/);
                res.text.should.match(/APPLE/);
                res.text.should.match(/NIKE/);
                res.text.should.match(/FORD/);
                res.text.should.match(/DEFAULT/);
              done();
            });
      });
  });

  describe('/GET product', () => {
      it('it should GET all products', (done) => {
        chai.request(app)
            .get('/product')
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.html;
                res.text.should.match(/Classic/);
                res.text.should.match(/Standout/);
                res.text.should.match(/Premium/);
              done();
            });
      });
  });

  describe('/GET sample-carts/unilever', () => {
      it('it should GET unilever-cart', (done) => {
        chai.request(app)
            .get('/sample-cart/unilever')
            .end((err, res) => {
                res.should.have.status(200);
                res.text.should.match(/UNILEVER CART/);
                res.should.be.html;
              done();
            });
      });
  });

  describe('/GET sample-carts/apple', () => {
      it('it should GET apple-cart', (done) => {
        chai.request(app)
            .get('/sample-cart/apple')
            .end((err, res) => {
                res.should.have.status(200);
                res.text.should.match(/APPLE CART/);
                res.should.be.html;
              done();
            });
      });
  });

   describe('/GET sample-carts/nike', () => {
      it('it should GET nike-cart', (done) => {
        chai.request(app)
            .get('/sample-cart/nike')
            .end((err, res) => {
                res.should.have.status(200);
                res.text.should.match(/NIKE CART/);
                res.should.be.html;
              done();
            });
      });
  });
   describe('/GET sample-carts/ford', () => {
      it('it should GET ford-cart', (done) => {
        chai.request(app)
            .get('/sample-cart/ford')
            .end((err, res) => {
                res.should.have.status(200);
                res.text.should.match(/FORD CART/);
                res.should.be.html;
              done();
            });
      });
  });

  describe('/GET sample-carts/default', () => {
      it('it should GET default-cart', (done) => {
        chai.request(app)
            .get('/sample-cart/default')
            .end((err, res) => {
                res.should.have.status(200);
                res.text.should.match(/DEFAULT CART/);
                res.should.be.html;
              done();
            });
      });
  });
  
   describe('/GET UNDEFINED PATH', () => {
      it('404 for everything else', (done) => {
        chai.request(app)
          .get('/foo/undef')
          .end((err, res) => {
                res.should.have.status(404);
                done();
            });
      });
  });
  