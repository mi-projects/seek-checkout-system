/*============== Unit Test - Discount Calculation Feature ==============*/

var expect  = require("chai").expect;

var DiscountCalculator = require('../discount/discount-calculator');

var unilever = require('./test-data/unilever');
var apple = require('./test-data/apple');
var nike = require('./test-data/nike');
var ford = require('./test-data/ford');
var defaultCustomer = require('./test-data/default');



describe("Discount Calculator", function(){
    describe("N for one less deal on a product", function() {
        it('should return 934.97', function(){
            
            var customer = unilever.unileverTestData;
            var discountCalculator = new DiscountCalculator(customer, customer.shoppingCart.items);
            var discountAmount = discountCalculator.applyPolicy();
            var finalPrice =  parseFloat((customer.shoppingCart.totalPrice - discountAmount).toFixed(2));

            expect(finalPrice).to.equal(934.97);
        });
    });

    describe("Price Drops to a fixed price per product", function() {
        it('should return 1294.96', function(){
            
            var customer = apple.appleTestData;
            var discountCalculator = new DiscountCalculator(customer, customer.shoppingCart.items);
            var discountAmount = discountCalculator.applyPolicy();
            var finalPrice =  parseFloat((customer.shoppingCart.totalPrice - discountAmount).toFixed(2));

            expect(finalPrice).to.equal(1294.96);
        });
    });

    describe("Discount for N or more purchase of a product", function() {
        it('should return 1519.96', function(){
           
            var customer = nike.nikeTestData;
            var discountCalculator = new DiscountCalculator(customer, customer.shoppingCart.items);
            var discountAmount = discountCalculator.applyPolicy();
            var finalPrice =  parseFloat((customer.shoppingCart.totalPrice - discountAmount).toFixed(2));
           
            expect(finalPrice).to.equal(1519.96);
        });
    });

    describe("Multiple discount policies combined", function() {
        it('should return 3259.90', function(){
            
            var customer = ford.fordTestData;
            var discountCalculator = new DiscountCalculator(customer, customer.shoppingCart.items);
            var discountAmount = discountCalculator.applyPolicy();
            var finalPrice =  parseFloat((customer.shoppingCart.totalPrice - discountAmount).toFixed(2));

            expect(finalPrice).to.equal(3259.90);
        });
    });

     describe("No Discount for non-privileged customers", function() {
        it('should return 987.97', function(){
            
            var customer = defaultCustomer.defaultTestData;
            var discountCalculator = new DiscountCalculator(customer, customer.shoppingCart.items);
            var discountAmount = discountCalculator.applyPolicy();
            var finalPrice =  parseFloat((customer.shoppingCart.totalPrice - discountAmount).toFixed(2));

            expect(finalPrice).to.equal(987.97);
        });
    });

    describe("DiscountCalculator with empty parameters", function() {
        it('should return 0', function(){
    
            var discountCalculator = new DiscountCalculator();
            var discountAmount = discountCalculator.applyPolicy();

            expect(discountAmount).to.equal(0);
        });
    });

});
